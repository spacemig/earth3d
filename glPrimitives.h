/*
 * Released under MIT License http://opensource.org/licenses/MIT
 * Refer to file LICENSE or URL above for full text
 *
 * Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
 * https://bitbucket.org/spacemig/earth3d
 *
 */

#ifndef OpenGLPrimitive_H
#define OpenGLPrimitive_H

// later it would be good to remove the Qt dependency from here
// hum ... I think I'm leaving this with Qt dependency because after
// all this is a graphical application and it would require some other
// lib like GLFW or SDL to make it work, sounds fair?

//#include <QGLWidget> // for GLfloat type
#include <QOpenGLWidget>
#include <QOpenGLBuffer>

#include <vector>
#define _USE_MATH_DEFINES
#include <cmath>

// add GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
//#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtx/matrix_operation.hpp>


class VBO{
public:
    QOpenGLBuffer vertex;
    QOpenGLBuffer color;
};

class OpenGLPrimitive
{
public:
    OpenGLPrimitive();
    //void Cube();
    VBO vbo;
    std::vector<glm::vec3> vertices, colors;
    QString type;
    void setupVbo(OpenGLPrimitive primitive);
};

struct Vertex {
    float x, y, z;
};

struct Color {
    float r, g, b;
};

class Triangle : public OpenGLPrimitive{

public:
    Triangle();
    //int numVertices();

    //    const GLfloat vertex_data[27] = {
    //        -5.0f, -5.0f, 0.0f,
    //        5.0f, -5.0f, 0.0f,
    //        0.0f,  0.0f, 0.0f,

    //        //        1.0f, 0.0f, 0.0f,
    //        //        3.0f, 0.0f, 0.0f,
    //        //        2.0f, 1.0f, 0.0f,

    //        //        3.0f, 0.0f, 0.0f,
    //        //        4.0f, 0.0f, 0.0f,
    //        //        3.0f, 1.0f, 0.0f,
    //    };

    //    const GLfloat color_data[27] = {
    //        1.0f,  0.0f,  0.0f,
    //        0.0f,  1.0f,  0.0f,
    //        0.0f,  0.0f,  1.0f,

    //        //        0.822f,  0.569f,  0.201f,
    //        //        0.435f,  0.602f,  0.223f,
    //        //        0.310f,  0.747f,  0.185f,

    //        //        0.597f,  0.770f,  0.761f,
    //        //        0.559f,  0.436f,  0.730f,
    //        //        0.359f,  0.583f,  0.152f,
    //    };

};





class Cylinder : public OpenGLPrimitive
{

public:
    Cylinder();
    Cylinder(float r, float height, glm::vec3 color);
    Cylinder(Vertex base_center, Vertex direction, float radius, float height, glm::vec3 color);


    void setup(float r, float height, glm::vec3 color);
    void setupPanel(Vertex base_a, Vertex base_b, float height);
};



class Cone : public OpenGLPrimitive
{
public:
    Cone();
    Cone(float r, float height, float cone_height);
    Cone(Vertex base_center, Vertex direction, float base_radius, float height, glm::vec3 color);
};

class Vector : public OpenGLPrimitive
{

public:
    Vector();
    Vector(Vertex a, Vertex b, float r, glm::vec3 color);
    void setupAB(Vertex a, Vertex b, float r, glm::vec3 color);
    void setupVector(Vertex position, Vertex vector, float r, glm::vec3 color);
};


class ReferenceFrame : public OpenGLPrimitive
{
    Vector vectorAxisX, vectorAxisY, vectorAxisZ ;
    //float size = 1; // default size
public:
    ReferenceFrame();
    void setReferenceFrame(float size);
};


class Line : public OpenGLPrimitive
{

public:
    Line();
    Line(Vertex, Vertex);
    void updateVertices(Vertex a, Vertex b);
    void updateColors(Color a, Color b);
    void addVertex(Vertex v, Color c);

    //    std::vector<glm::vec3> vertices, color;

    //    //
    //    QOpenGLBuffer mVboVertex;
    //    QOpenGLBuffer mVboColor;
};


class Cube{

public:
    Cube();
    //int numVertices();
    //std::vector<double> vd {1.2,3};
};

#endif // OpenGLPrimitive_H
