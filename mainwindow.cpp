/*
 * Released under MIT License http://opensource.org/licenses/MIT
 * Refer to file LICENSE or URL above for full text
 *
 * Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
 * https://bitbucket.org/spacemig/earth3d
 *
 */
#include "mainwindow.h"
#include "sphere.h"

#include <QGridLayout>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

    setWindowTitle(tr("3D Orbit View"));

    // Set size
    //QSize w_size(800, 420);

    //    QGridLayout *mainLayout = new QGridLayout;

    //    WidgetTriangle *widgetTriangle = new WidgetTriangle;
    //    widgetTriangle->setMinimumWidth(500);
    //    widgetTriangle->setMinimumHeight(500);
    //    setCentralWidget(widgetTriangle);

    //    WidgetCube *widgetCube = new WidgetCube;
    //    setCentralWidget(widgetCube);

    Sphere *sphere = new Sphere;


    // Set layout

    QGridLayout *layout = new QGridLayout;

    QLabel *nativeLabel = new QLabel(tr("Native"));
    nativeLabel->setAlignment(Qt::AlignHCenter);

    layout->addWidget(sphere, 0, 0);
    layout->addWidget(nativeLabel, 1, 0);

    // Set layout in Qwidget
    QWidget *window = new QWidget();
    window->setLayout(layout);

    //    setCentralWidget(window);
    setCentralWidget(sphere);

    //    GlobeWidget *widgetGlobe = new GlobeWidget();
    //    setCentralWidget(widgetGlobe);

    //    mainLayout->addWidget(widgetGlobe,0,0);
    //    setCentralWidget(widgetGlobe);
    //    gridLayout->addLayout(gridLayoutOpenGlWindows   ,2, 0, 1,1);

    //    setCentralWidget(gridLayout);

    //    QTimer *timer = new QTimer(this);

    //    timer->setInterval(10);

    //    QGroupBox * groupBox = new QGroupBox(this);
    //    setCentralWidget(groupBox);

    //    QVBoxLayout *gLayout = new QVBoxLayout();
    //    gLayout->addWidget(glwidget);

    //    QHBoxLayout *layout = new QHBoxLayout(groupBox);
    //    layout->addLayout(gLayout);

    //    QObject::connect(timer, SIGNAL(timeout()), glwidget, SLOT(paintGL()));

    //    timer->start();
}

MainWindow::~MainWindow()
{

}
