/*
 * Released under MIT License http://opensource.org/licenses/MIT
 * Refer to file LICENSE or URL above for full text
 *
 * Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
 * https://bitbucket.org/spacemig/earth3d
 *
 */
#include <QApplication>
#include <QSurfaceFormat>

#include "mainwindow.h"

int main( int argc, char *argv[] )
{
    // for loading the texture
    Q_INIT_RESOURCE(texture);

    QApplication a( argc, argv );

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    //format.setVersion(3,3);
    format.setSamples(4); // renders 4 times each pixel
    //    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow mw;
    //mw.showMaximized();
    mw.setMinimumSize(400,400);
    mw.show();

    return a.exec();
}
