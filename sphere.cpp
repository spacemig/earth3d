/*
 * Released under MIT License http://opensource.org/licenses/MIT
 * Refer to file LICENSE or URL above for full text
 *
 * Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
 * https://bitbucket.org/spacemig/earth3d
 *
 */

#include "sphere.h"

// QtOpenGl
#include <QOpenGLContext>
#include <QSurfaceFormat>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
// Other Qt libs
#include <QMouseEvent>
#include <QTimer>
#include <QPainter>

#include <iostream>
#include <cmath>
using namespace std;



Sphere::Sphere(QWidget *parent) :
    QOpenGLWidget(parent),
    vboIndices(QOpenGLBuffer::IndexBuffer),
    texture(0),
    //eye(5,5,2),
    center(0,0,0),
    unitX(1,0,0),
    unitZ(0,0,1)
{


    sphereRadius = 6000e3;
    eyeDistance = sphereRadius*5;
    //eye = QVector3D(30,30,2);

    //rotation = QQuaternion::fromAxisAndAngle(QVector3D(0,1,0), 45);

    //    eye = QVector3D(5,0,0);
    //vMatrix.lookAt(eye,center,unitZ);
    //    MVP.translate(0.0, 0.0, -10.0);
    //    MVP.rotate(rotation);

    //--------------------------------------------------------------
    QTimer *timer = new QTimer(this);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(16);

    //    painter.begin(this);


}

Sphere::~Sphere()
{
}


void Sphere::initializeGL()
{

    initializeOpenGLFunctions();

    // Dark blue background
    glClearColor(0.1f, 0.1f, 0.3f, 0.0f);

    initShaders(&shTexture,
                ":/shaders/texture.vsh",
                ":/shaders/texture.fsh");
    initShaders(&shColor,
                ":/shaders/color.vsh",
                ":/shaders/color.fsh");

    initTextures();

    //    glEnable(GL_DEPTH_TEST); // Enable depth buffer
    //    glEnable(GL_CULL_FACE); // Enable back face culling


    // Vertex Array Object
    //m_vao = new QOpenGLVertexArrayObject(this);
    vao.create();


    // define geometry and create VBOs
    makeSphere();

    //InitializeVertexBuffer();

    //printOpenGLinfo();

    frameInertial.setReferenceFrame(sphereRadius*2);
    frameInertial.setupVbo(frameInertial); // change to only setupVbo()


}

void Sphere::paintGL()
{



    //    glMatrixMode(GL_MODELVIEW);
    //    glPushMatrix();

    // used for QPaintEvent
    //    makeCurrent();
    vao.bind();


    //    glClearColor(0.0f, 0.0f, 0.0f, 0.0f );

    glEnable(GL_DEPTH_TEST); // Enable depth buffer
    glEnable(GL_CULL_FACE); // Enable back face culling

    // Clear the screen
    //     glClear( GL_COLOR_BUFFER_BIT );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    //        glClear( GL_DEPTH_BUFFER_BIT );

    // compute MVP
    QMatrix4x4 cameraTransformation;
    cameraTransformation.rotate(alpha, 0, 0, 1);
    cameraTransformation.rotate(beta, 0, 1, 0);
    QVector3D cameraPosition = cameraTransformation * QVector3D(eyeDistance, 0, 0);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 0, 1);

    vMatrix.setToIdentity();
    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);


    //        qDebug() << "cameraPosition = " << cameraPosition;
    //    qDebug() << "cameraUpDirection = " << cameraUpDirection;

    MVP = pMatrix * vMatrix * mMatrix;


    mMatrix_sphere.setToIdentity();
    mMatrix_sphere.rotate(180,unitZ);
    MVP_sphere = pMatrix * vMatrix * mMatrix_sphere;
    //    qDebug() << "pMatrix = " << pMatrix;
    //    qDebug() << "vMatrix = " << vMatrix;
    //    qDebug() << "mMatrix = " << mMatrix;

    // ----------------------
    // Draw Sphere
    // Set modelview-projection matrix
    shTexture.bind();
    shTexture.setUniformValue("MVP", MVP_sphere); //pMatrix * MVP

    texture->bind(); // Tell OpenGL which VBO to use
    shTexture.setUniformValue("texture", 0); // Use texture unit 0

    vboTexture.bind(); // Tell OpenGL which VBO to use
    shTexture.enableAttributeArray("a_texcoord");
    shTexture.setAttributeArray("a_texcoord", GL_FLOAT, 0, 2);
    vboTexture.release();

    vboPosition.bind(); // Tell OpenGL which VBO to use
    shTexture.enableAttributeArray("vertexPosition");
    shTexture.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    vboPosition.release();


    //
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices.bufferId());
    vboIndices.bind();

    // draw it finally!
    //glDrawArrays(GL_TRIANGLES, 0, 6);
    //glDrawArrays(GL_LINE_STRIP, 0, 6); // only draw lines

    glDrawElements( GL_TRIANGLES, indices.count(), GL_UNSIGNED_INT, NULL );

    vboIndices.release();
    //    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE ); // only draw lines

    shTexture.disableAttributeArray("a_texcoord");
    shTexture.disableAttributeArray("vertexPosition");

    shTexture.release();

    // ----------------------
    // Draw Reference Frames
    //draw(&m_shaderProgramSimple, frameInertial);
    shColor.bind();
    shColor.setUniformValue("MVP", MVP);

    frameInertial.vbo.vertex.bind();
    shColor.enableAttributeArray("vertexPosition");
    shColor.setAttributeArray("vertexPosition",GL_FLOAT,0, 3);
    frameInertial.vbo.vertex.release();

    frameInertial.vbo.color.bind();
    shColor.enableAttributeArray("vertexColor");
    shColor.setAttributeBuffer("vertexColor", GL_FLOAT, 0, 3);
    frameInertial.vbo.color.release();

    glDrawArrays(GL_TRIANGLES, 0, frameInertial.vertices.size());

    shColor.disableAttributeArray("vertexColor");
    shColor.disableAttributeArray("vertexPosition");

    shColor.release();

    vao.release();

    //    glShadeModel(GL_FLAT);

    //    glDisable(GL_CULL_FACE);
    //    glDisable(GL_DEPTH_TEST);
    //            glMatrixMode(GL_MODELVIEW);
    //    glPopMatrix();

    // used for QPaintEvent
    //    doneCurrent();



}

void Sphere::paintEvent(QPaintEvent * event){
    //void Sphere::paintGL(){
    //cout << "hi" << endl;

    makeCurrent();


    //    p.begin();
    //    p.setRenderHint(QPainter::Antialiasing);
    //    p.setBackgroundMode(Qt::TransparentMode); // < was a test, doesn't seem to do anything
    //    p.setBackground(QColor(0,0,0,0));

    paintGL();

    //    glClear (GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    glDisable (GL_DEPTH_TEST);

    QPainter p(this);
    p.setPen(Qt::yellow);
    p.setFont(QFont("Arial", 10));
    p.drawText(rect(), Qt::AlignLeft, "Qt Text");
    //    p.drawText(rect(), Qt::AlignRight, "fps:");
    p.end();

    doneCurrent();
    //    update();

}

void Sphere::resizeGL(int w, int h){
    //glViewport(0, 0, w, h);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
    //const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;
    const qreal zNear = 100, zFar = 100000e3, fov = 60.0;

    // Reset projection
    pMatrix.setToIdentity();

    // Set perspective projection
    pMatrix.perspective(fov, aspect, zNear, zFar);

}



void Sphere::initShaders(QOpenGLShaderProgram *shaderProgram,
                         QString vertexShaderFile,
                         QString fragmentShaderFile)
{

    // Compile vertex shader
    if (!shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile)){
        //QMessageBox::warning(this, "QOpenGLShader::Vertex", "QOpenGLShader::Vertex" + program.log());
        qCritical() << QObject::tr("Could not compile vertex shader. Log: ") << shaderProgram->log();
        close();
    }

    // Compile fragment shader
    if (!shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile)){
        //QMessageBox::warning(this, "QOpenGLShader::Fragment", "QOpenGLShader::Fragment" + program.log());
        qCritical() << QObject::tr("Could not compile fragment shader. Log: ") << shaderProgram->log();
        close();
    }

    // Link shader pipeline
    if (!shaderProgram->link()){
        qCritical() << QObject::tr("Could not link shader program. Log: ") << shaderProgram->log();
        close();
    }

    // Bind shader pipeline for use
    // equivalent to use glUseProgram(programID);
    if (!shaderProgram->bind())
        close();

}


void Sphere::initTextures()
{
    //texture = new QOpenGLTexture(QImage(":/textures/side1.png").mirrored());
    texture = new QOpenGLTexture(QImage(":/textures/earth.png"));


    //    // Set nearest filtering mode for texture minification
    //    texture->setMinificationFilter(QOpenGLTexture::Nearest);

    //    // Set bilinear filtering mode for texture magnification
    //    texture->setMagnificationFilter(QOpenGLTexture::Linear);

    //    // Wrap texture coordinates by repeating
    //    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    //    texture->setWrapMode(QOpenGLTexture::Repeat);
}

void Sphere::makeSphere(){

    int stacks = 32;
    int slices = 32;
    //float radius = 2;

    const float pi = 3.1415926535897932384626433832795f;
    const float _2pi = 2.0f * pi;

    for( int i = 0; i <= stacks; ++i )
    {
        // V texture coordinate.
        float V = i / (float)stacks;
        float phi = V * pi;

        for ( int j = 0; j <= slices; ++j )
        {
            // U texture coordinate.
            float U = j / (float)slices;
            float theta = U * _2pi;

            float X = cos(theta) * sin(phi);
            float Y = sin(theta) * sin(phi);
            float Z = cos(phi); // principal axis

            positions.push_back( QVector3D( X, Y, Z) * sphereRadius );
            normals.push_back( QVector3D(X, Y, Z) );
            textureCoords.push_back( QVector2D(U, V) );
        }

    }

    // Now generate the index buffer
    // iterate trhough each face of the sphere
    for( int i = 0; i < slices * stacks + slices; ++i )
    {
        // first triangle, upper
        indices.push_back( i );
        indices.push_back( i + slices );
        indices.push_back( i + slices + 1  );


        // second triangle, lower
        indices.push_back( i + slices + 1  );
        indices.push_back( i + 1 );
        indices.push_back( i );

    }

    // setup VBO for position
    vboPosition.create();
    vboPosition.bind();
    //vboPosition.allocate(vertData.constData(), vertData.count() * sizeof(QVector3D));
    vboPosition.allocate(positions.constData(), positions.count() * sizeof(QVector3D));
    vboPosition.release();

    // setup VBO for normals
    vboNormals.create();
    vboNormals.bind();
    vboNormals.allocate(normals.constData(), normals.count() * sizeof(QVector3D));
    vboNormals.release();

    // setup VBO for texture
    vboTexture.create();
    vboTexture.bind();
    vboTexture.allocate(textureCoords.constData(), textureCoords.count() * sizeof(QVector2D));
    vboTexture.release();

    // setup VBO for indices
    vboIndices.create();
    vboIndices.bind();
    vboIndices.allocate(indices.constData(), indices.count() * sizeof(GLfloat));
    vboIndices.release();
}





void Sphere::mousePressEvent(QMouseEvent *event)
{
    //    qDebug() << event->pos();

    //store the mouse pointer’s initial position to be able to
    //track the movement
    mouseLastPos = event->pos();
    //mouseLastPos.setX(event->pos().x());
    //mouseLastPos.setY(event->pos().y());

    //event->accept();

}

void Sphere::mouseMoveEvent(QMouseEvent *event)
{
    //    qDebug() << event->pos();

    QPoint mousePos(event->pos().x(), event->pos().y());

    float delta_x = mousePos.x() - mouseLastPos.x();
    float delta_y = mousePos.y() - mouseLastPos.y();

    // ---------------------------------------------------
    // from Qt OpenGL tutorial, pg 23
    // source: ftp://ftp.informatik.hu-berlin.de/pub/Linux/Qt/QT/developerguides/qtopengltutorial/OpenGLTutorial.pdf


    alpha -= delta_x/5.;
    while (alpha < 0) {
        alpha += 360;
    }
    while (alpha >= 360) {
        alpha -= 360;
    }

    beta -= delta_y/5.;
    if (beta < -90) {
        beta = -90;
    }
    if (beta > 90) {
        beta = 90;
    }

    //    qDebug() << "alpha: " << alpha << "beta: " << beta;

    mouseLastPos = mousePos;

    update();


    //    // trackball motion
    //    // ref: https://www.opengl.org/wiki/Object_Mouse_Trackball

    //    float r = zoomFactor;
    //    float cR = 0;

    //    float x1 = view_x;
    //    float y1 = view_y;
    //    float z1 = 0;

    //    cR = x1*x1 + y1*y1;
    //    if (cR <= r*r/2.0f){
    //        z1 = sqrt(r*r - cR);
    //    } else {
    //        z1 = (r*r/2.0f)/sqrt(cR);
    //    }

    //    float x2 = view_x + (delta_x)/100;
    //    float y2 = view_y + (delta_y)/100;
    //    float z2 = 0;

    //    cR = x2*x2 + y2*y2;
    //    if (cR <= r*r/2.0f){
    //        z2 = sqrt(r*r - (cR));
    //    } else {
    //        z2 = (r*r/2.0f)/sqrt(cR);
    //    }

    //    QVector3D v1(x1,y1,z1);
    //    QVector3D v2(x2,y2,z2);

    //    // >> normalize vector
    //    QVector3D N =  QVector3D::crossProduct(v1,v2);
    //    float dot = QVector3D::dotProduct(v1,v2);
    //    float theta = acos(dot/(v1.length()*v2.length()));
    //    //float theta1 = acos(dot);

    //    N = QVector3D(0,0,-1);
    //    float theta_x =  -(delta_x)/100;
    //    float theta_y =  -(delta_y)/100;
    //    theta = theta_x + theta_y;

    //    QVector3D mouseAxisX(0,0,-1);
    //    QVector3D viewFrom(view_x,view_y,view_z);
    //    QVector3D mouseAxisY = QVector3D::crossProduct(mouseAxisX,viewFrom);
    //    mouseAxisY.normalize();

    //    QVector3D moveX = mouseAxisX*theta_x;
    //    QVector3D moveY = mouseAxisY*theta_y;

    //    QVector3D Nnew = moveX + moveY;


    //    //QQuaternion q = QQuaternion::fromAxisAndAngle(Nnew, -1.4f);

    //    // new

    //    // convert pixel to view position in unitary shpere
    //    QPointF viewLastPos(2.0 * float(mouseLastPos.x()) / width() - 1.0,
    //                        1.0 - 2.0 * float(mouseLastPos.y()) / height());

    //    QPointF viewPos(2.0 * float(mousePos.x()) / width() - 1.0,
    //                    1.0 - 2.0 * float(mousePos.y()) / height());

    //    //    QVector3D lastPos3D = QVector3D(viewLastPos.x(), viewLastPos.y(), 0.0f);
    //    QVector3D lastPos3D = QVector3D(0.0f, viewLastPos.x(), viewLastPos.y());
    //    float sqrZ = 1.0 - QVector3D::dotProduct(lastPos3D, lastPos3D);
    //    if (sqrZ > 0)
    //        //        lastPos3D.setZ(sqrt(sqrZ));
    //        lastPos3D.setX(sqrt(sqrZ));
    //    else
    //        lastPos3D.normalize();

    //    //    QVector3D currentPos3D = QVector3D(viewPos.x(), viewPos.y(), 0.0f);
    //    QVector3D currentPos3D = QVector3D(0.0f, viewPos.x(), viewPos.y());
    //    sqrZ = 1.0 - QVector3D::dotProduct(currentPos3D, currentPos3D);
    //    if (sqrZ > 0)
    //        //currentPos3D.setZ(sqrt(sqrZ));
    //        currentPos3D.setX(sqrt(sqrZ));
    //    else
    //        currentPos3D.normalize();

    //    QVector3D axis;
    //    axis = QVector3D::crossProduct(lastPos3D, currentPos3D);
    //    QQuaternion q = QQuaternion::fromAxisAndAngle(axis, 2);
    //    q.normalize();

    //MVP.rotate(q);

    //    qDebug() << MVP;
    //    MVP.setToIdentity();
    //    eye = QVector3D(5,0,0);
    //    MVP.lookAt(eye, center, unitZ);

    //    mouseLastPos = mouseLastPos;

    //    MVP.setToIdentity();
    //    qDebug() << MVP;
    //    qDebug() << "last pos: " << mouseLastPos << " | curr pos: " << mousePos;
    //    qDebug() << "last pos3D: " << lastPos3D << " | curr pos 3D: " << currentPos3D;

    //    QQuaternion q_test = QQuaternion::fromAxisAndAngle(QVector3D(0,0,1), 45);
    //    QMatrix4x4 m_test;
    //    m_test.rotate(q_test);
    //    qDebug() << m_test;

    //    qDebug() << MVP;

    //    QMatrix3x3 m;
    //    q.
    //    viewFrom.;
    //    glm::vec3 v = glm::toMat3(q)*glm::vec3(view_x,view_y,view_z);
    //    view_x = v.x;
    //    view_y = v.y;
    //    view_z = v.z;


}

void Sphere::wheelEvent(QWheelEvent *event)
{

    int delta = event->delta();
    if (event->orientation() == Qt::Vertical) {
        if (delta < 0) {
            eyeDistance *= 1.1;
        } else if (delta > 0) {
            eyeDistance *= 0.9;
        }
        update();
    }
    //    event->accept();

}
