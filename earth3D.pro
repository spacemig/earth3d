#--------------------------------------------------------------------
# Qt project file for Earth3D
# A simple OpenGL program to draw a Sphere with Texture (Earth Map)
# with OpenGL shaders.
#
# Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
# https://bitbucket.org/spacemig/earth3d
#
# Released under MIT License http://opensource.org/licenses/MIT
# Refer to file LICENSE or URL above for full text
#--------------------------------------------------------------------


message("----------------------------------------------------------")
message("Compiling Earth3D")


QT += core gui opengl

TEMPLATE = app
#TARGET = globe
CONFIG += c++11

# Input
SOURCES += main.cpp \

SOURCES += mainwindow.cpp
HEADERS += mainwindow.h

SOURCES += sphere.cpp
HEADERS += sphere.h

SOURCES += glPrimitives.cpp
HEADERS += glPrimitives.h

RESOURCES += texture.qrc
RESOURCES += resources.qrc

PROJECT = $$PWD

MODULES += GLM
include( $$PROJECT/thirdparty/thirdparty.pri )

