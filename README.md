# EARTH 3D #

A simple OpenGL program in Qt 5.4 to draw a Sphere with Texture (Earth Map) with OpenGL shaders.

I got it fixed!!!! It was all about the buffers. I had to release the buffers in the OpenGL code and also use glDisable(GL_DEPTH_TEST) before using the painter.
 
Old:
Right now I am trying to print 2D text with QPainter on top of the OpenGL window. I'm getting weird stuff, check the following images. The first one is when the program starts. But when I rotate the globe with the mouse it looks really bad (image 2).

![Image 1](https://bytebucket.org/spacemig/earth3d/raw/59a577b08f55dc1c48be2cb2f2da4ac3ed482aa0/painter-problem/opengl-with-painter1.PNG)

![Image 2](https://bitbucket.org/spacemig/earth3d/raw/59a577b08f55dc1c48be2cb2f2da4ac3ed482aa0/painter-problem/opengl-with-painter2.PNG)