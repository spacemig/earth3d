/*
 * Released under MIT License http://opensource.org/licenses/MIT
 * Refer to file LICENSE or URL above for full text
 *
 * Copyright (c) 2015 Miguel A. Nunes (spacemig@gmail.com)
 * https://bitbucket.org/spacemig/earth3d
 *
 */

#ifndef Sphere_H
#define Sphere_H

#include <QWindow>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QPainter>

#include "glPrimitives.h"

class Sphere : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT // must include this if you use Qt signals/slots

public:
    Sphere(QWidget *parent= 0);
    ~Sphere();

    void printOpenGLinfo();


protected:
    // -- main 3 opengl functions
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    //
    void paintEvent(QPaintEvent *event);

    void initShaders(QOpenGLShaderProgram *shaderProgram,
                     QString vertexShaderFile,
                     QString fragmentShaderFile);
    void initTextures();

    void makeSphere();

    //void InitializeVertexBuffer();


private:
    QOpenGLShaderProgram shTexture;
    QOpenGLShaderProgram shColor;

    GLuint vertexbuffer;

    QOpenGLVertexArrayObject vao;
    QOpenGLBuffer vboPosition;
    QOpenGLBuffer vboColor;
    QOpenGLBuffer vboNormals;
    QOpenGLBuffer vboTexture;
    QOpenGLBuffer vboIndices;

    GLuint m_posAttr;
    GLuint m_colAttr;

    QOpenGLTexture *texture;


    QQuaternion rotation;

    QVector<QVector3D> positions;
    QVector<QVector3D> normals;
    QVector<QVector2D> textureCoords;
    QVector<GLuint> indices;

    // mouse interaction
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

    QPoint mouseLastPos;
    float zoomFactor = 5;

    // initial perspective
    float view_x = 5;
    float view_y = 0;
    float view_z = 0;

    // sphere
    float sphereRadius = 1;

    // model view projection matrix
    QMatrix4x4 MVP;

    QMatrix4x4 mMatrix; // model
    QMatrix4x4 pMatrix; // projection
    QMatrix4x4 vMatrix; // view
    //QMatrix4x4 projection;

    QMatrix4x4 MVP_sphere;
    QMatrix4x4 mMatrix_sphere; // model

    QVector3D eye;
    QVector3D center;

    QVector3D unitX;
    QVector3D unitZ;

    int counter = 0;

    QTimer *timer;

    // Reference Frame
    ReferenceFrame frameInertial;


    // mouse operation
    float alpha = 0;
    float beta = 0;
    float eyeDistance = 5;

    // 2D text
    QPainter painter;

};

#endif // Sphere_H
